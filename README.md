# What is this ?

this is a fork of example compositor based on [Louvre](https://github.com/CuarzoSoftware/Louvre). I just made a litte changing on its source code, such as gaps on maximized window and clear keyboard focus for minimized window. 

## Future features.

I'm tinkering my own compositor / [TinyWay](https://gitlab.com/lidgl/tinyway) and I hope to bring some its features to this compositor, such as :

- Window Manipulations :
    - Maximize vertical / split.
    - Resizing / moving window via keybind.
    - Placing window at specific area.

## Additional tinkering.

I'm a laptop user with touchpad (obviously), this compositor has not tap-to-click support by default, using this [library](https://gitlab.com/warningnonpotablewater/libinput-config) will help us to get tap-to-click feature.

You can create a file named libinput.conf and place it under /etc. This is my minimal config :

```
override-compositor=enabled
tap=enabled
dwt=disabled
#scroll-factor=0.175
drag-lock=enabled
```
